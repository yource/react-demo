import React from 'react';
import {useNavigate} from "react-router-dom";
import './style.scss';

function NotFoundPage() {
	const navigate = useNavigate();
	const navBack = ()=>{
		if(window.localStorage.getItem("token")){
			navigate("/firstPage")
		}else{
			navigate("/login")
		}
	}
    return (
        <div className="NotFoundPage">
			<h1>404</h1>
			<div onClick={navBack} className="mybtn">BACK</div>
		</div>
    );
}

export default NotFoundPage;
