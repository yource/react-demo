import {createSlice} from '@reduxjs/toolkit';

var initialState = {
	price:0
};

export const orderSlice = createSlice({
	name: 'order',
	initialState,
	reducers: {
		changePriceAction: (state, action) => {
			state.price += Number(action.payload)
		},
	},
});

export const {changePriceAction} = orderSlice.actions;
export const selectOrder = (state) => state.order;
export default orderSlice.reducer;
