import style from "./style.module.scss";
import React,{useState, useEffect} from "react";
import closeIcon from "../../assets/icon/close.png";

export default function CustomDialog(props){
	const [show,setShow] = useState(false);
	const [display,setDisplay] = useState(false);
	
	const closeDialog = ()=>{
		setShow(false);
		setTimeout(()=>{
			setDisplay(false);
			props.onClose()
		},200)
	}
	
	const handleCancel = ()=>{
		if(typeof props.onCancel=='function'){
			props.onCancel()
		}
		closeDialog()
	}
	
	const handleOK = ()=>{
		if(typeof props.onOK=='function'){
			props.onOK()
		}
		closeDialog()
	}
	
	useEffect(()=>{
		if(props.show){
			setDisplay(true);
			setTimeout(()=>{
				setShow(true)
			},20)
		}else if(show){
			closeDialog()
		}
	},[props.show])
	return (
		display?(
			<div className={`${style.my_custom_dialog_wrap} ${show?style.show:''}`}>
				<div className={style.my_custom_dialog}>
					<div className={style.title}>
						<span>{props.title}</span>
						<div className={style.closeBtn} onClick={handleCancel}>
							<img src={closeIcon} />
						</div>
					</div>
					<div className={style.content}>
						{props.children}
					</div>
					<div className={style.foot}>
						<div className={style.btn} onClick={handleOK}>OK</div>
						<div className={style.btn} onClick={handleCancel}>Cancel</div>
					</div>
				</div>
			</div>
		):null
	)
}