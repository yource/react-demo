/**
 * 刷新后需要保留的state数据 保存在localStorage/sessionStorage中
 * createSlice.reducers用**Action命名
 */

import {createSlice} from '@reduxjs/toolkit';

var localState = window.localStorage.getItem("user_state");

var initialState = localState ? JSON.parse(localState):{
	id: "",
	email: ""
};

export const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		loginAction: (state, action) => {
			state.id = action.payload.userId;
			state.email = action.payload.email;
			window.localStorage.setItem("token",action.token)
			window.localStorage.setItem("user_state",JSON.stringify(state))
		},
		logoutAction: (state) => {
			state = {
				id: "",
				email: ""
			};
			window.localStorage.removeItem("token")
			window.localStorage.removeItem("user_state")
		},
	},
});

export const {loginAction, logoutAction} = userSlice.actions;

export default userSlice.reducer;
