import "./style.scss";

/**
 * count>0的时候显示loading
 * show: count++
 * hide: count--
 */
var count = 0;
var showTimer;
var hideTimer;

function show(force){
	count++;
	if(count<1 && force===true){
		count = 1;
	}
	if(count<1){
		count = 0;
		return;
	}
	var dom = window.document.getElementById("my_loading_wrap");
	if(!dom){
		dom = window.document.createElement("DIV");
		dom.id = "my_loading_wrap";
		dom.innerHTML = `<div class="my_loading">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: #FFF; display: block;" width="80px" height="80px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
			<circle cx="50" cy="50" r="30" stroke="#e6e7ea" stroke-width="10" fill="none"></circle>
			<circle cx="50" cy="50" r="30" stroke="#1fbfb4" stroke-width="8" stroke-linecap="round" fill="none">
			  <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;180 50 50;720 50 50" keyTimes="0;0.5;1"></animateTransform>
			  <animate attributeName="stroke-dasharray" repeatCount="indefinite" dur="1s" values="18.84955592153876 169.64600329384882;94.2477796076938 94.24777960769377;18.84955592153876 169.64600329384882" keyTimes="0;0.5;1"></animate>
			</circle>
			</svg>
		</div>`
		window.document.body.appendChild(dom);
	}
	if(hideTimer){
		window.clearTimeout(hideTimer)
	}
	if(!showTimer){
		dom.style.display = "block";
		showTimer = setTimeout(()=>{
			dom.classList.add("show");
			showTimer = null;
		},20)
	}
}

function hide(force){
	count--;
	if(count>0&&force===true){
		count = 0;
	}
	if(count>0){
		return;
	}
	var dom = window.document.getElementById("my_loading_wrap");
	if(dom){
		if(showTimer){
			window.clearTimeout(showTimer)
		}
		if(!hideTimer){
			dom.classList.add("hide");
			hideTimer = setTimeout(()=>{
				dom.style.display="none";
				dom.classList.remove("hide");
				dom.classList.remove("show");
				hideTimer = null;
			},150)
		}
	}
}
window.loading = {show,hide}
export default {show,hide}