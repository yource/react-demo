import { configureStore } from '@reduxjs/toolkit';
import user from './userSlice.js';
import order from './orderSlice.js';
import items from './itemsSlice.js';
import secondPage from '../pages/secondPage/slice.js'

export const store = configureStore({
  reducer: { user,order,items,secondPage },
});
