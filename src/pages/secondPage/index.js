import React, { useState, useRef }from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { changePriceAction, selectOrder} from '../../store/orderSlice.js';
import { selectSecondPage } from './slice.js';
import './style.scss';
import {loading,toast,dialog,Items,CustomDialog} from "../../components";
import Comp1 from "./comp1"
import Comp2 from "./comp2"
import logo from "../../assets/images/logo.svg"

function SecondPage() {
	const dispatch = useDispatch();

	// 获取、修改 全局store中的order数据
	const order = useSelector(selectOrder);
	const inputEl = useRef(null)
	const addOrderPrice = ()=>{
		dispatch(changePriceAction(inputEl.current.value||0))
	}
	const reduceOrderPrice = ()=>{
		dispatch(changePriceAction(0-(inputEl.current.value||0)))
	}
	
	// 获取前页面的数据
	const secondPageData = useSelector(selectSecondPage)
	
	const testToast = ()=>{
		toast("测试信息")
	}
	const testLoading = ()=>{
		loading.show();
		setTimeout(()=>{
			loading.hide()
		},2000)
	}
	const testDialog = ()=>{
		dialog.confirm({
			title:"测试",
			content:"通知提示aaa"
		})
	}
	
	const [customDialogShow,setCustomDialogShow] = useState(false);
	const testCustomDialog = ()=>{
		setCustomDialogShow(true)
	}
	
    return (
        <div className="secondPage">
			<div className="pageCon">
				<h1>SecondPage</h1>
				<div className="section section1">
					<h2>全局数据 order</h2>
					<p className="bigText">order price: {order.price}</p>
					<div className="editOrderPrice">
						<p>修改order.price: </p>
						<span onClick={addOrderPrice}>+</span>
						<input type="number" defaultValue="1" ref={inputEl}/>
						<span onClick={reduceOrderPrice}>-</span>
					</div>
				</div>
				<div className="section section2">
					<h2>跨页面组件 items</h2>
					<Items/>
				</div>
				<div className="section section3">
					<h2>页面数据 secondPage/slice.js</h2>
					<p className="bigText">Counter: {secondPageData.count}</p>
				</div>
				<div className="section section4">
					<h2>页面组件 comp1</h2>
					<Comp1 />
				</div>
				<div className="section section5">
					<h2>页面组件 comp2</h2>
					<Comp2 />
				</div>
				<div className="section section6">
					<h2>UI组件</h2>
					<button onClick={testToast}>toast</button>
					<button onClick={testLoading}>loading</button>
					<button onClick={testDialog}>dialog</button>
					<button onClick={testCustomDialog}>custom dialog</button>
				</div>
			</div>
			<CustomDialog title="提示" show={customDialogShow} 
				onClose={()=>{setCustomDialogShow(false)}}
				onOK={()=>{toast("点击了OK")}}
				onCancel={()=>{toast("关闭弹框")}}>
				<div className="customDialogCon">
					<p>这里是自定义内容</p>
					<p>这里是自定义内容</p>
					<p>这里是自定义内容</p>
					<img src={logo} />
				</div>
			</CustomDialog>
		</div>
    );
}

export default SecondPage;
