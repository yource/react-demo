import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import './style.scss';
import {login} from "../../api";
import {loading,toast} from "../../components";
import {loginAction} from "../../store/userSlice.js";
import logo from "../../assets/images/logo.svg";

export default function Counter() {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [emailErr, setEmailErr] = useState('');
	
	const submitLogin=()=>{
		if(!email){
			toast("请输入邮箱");
			return;
		}
		if(!password){
			toast("请输入密码");
			return;
		}
		if(!!emailErr){
			toast("邮箱格式错误");
			return;
		}
		loading.show();
		login({email,password,languageCode:200}).then((res)=>{
			dispatch(loginAction({...res,email}));
			navigate('/firstPage')
			loading.hide();
		},err=>{
			console.log(err)
			loading.hide();
		})
	}
	
	const handlerEmail=(val)=>{
		var reg = new RegExp('^[A-Za-z0-9]+([-_.][A-Za-z0-9]+)*@([A-Za-z0-9]+[-.])+[A-Za-z0-9]{2,4}$');
		setEmailErr(reg.test(val) ? "":"邮箱格式错误")
		setEmail(val)
	}
	
	return (
		<div>
			<div className="loginPage">
				<div className="loginForm">
					<img className="logo" src={logo} />
					<p>使用 yangxiang@menusifu.cn / Menusifu6788 登录</p>
					<div className="row">
						<div className="label">Emial: </div>
						<input type="email" value={email} onChange={(e)=>{handlerEmail(e.target.value)}} />
						{
							emailErr?(<div className="errTip">{emailErr}</div>):null
						}
					</div>
					<div className="row">
						<div className="label">Password: </div>
						<input type="password" defaultValue={password} onChange={(e)=>{setPassword(e.target.value)}} />
					</div>
					<div className="mybtn loginBtn" onClick={submitLogin}>SIGN IN</div>
				</div>
			</div>
		</div>
	);
}
