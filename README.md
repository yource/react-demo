## React 基本项目框架

src/index.js - 入口文件  
src/app/app.js - App主体框架  
src/app/app.scss - 全局样式  
src/app/routes.js - 路由文件  
src/components/loading&toast&dialog - 不依赖react的ui组件，原生js操作dom  
src/components/customDialog - 依赖react的ui组件  
src/components/items - 需要跨页面引用的业务组件  
src/pages/secondPage/index.js - 页面入口  
src/pages/secondPage/slice.js - 当前页面需要共享的数据  
src/pages/secondPage/comp1.js&comp2.js - 只在当前页面用到的组件  
src/store/index.js - 引入所有（全局的、页面的、组件的）reducer生成store  
src/store/userSlice&itemsSlice - 需要跨页面引用的全局数据  

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
