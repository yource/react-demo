import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
	addItemAction,
	removeItemAction,
	selectItems,
} from '../../store/itemsSlice';
import toast from '../toast'
import styles from './style.scss';

export default function Items() {
	const [currentInput,setCurrentInput] = useState('')
	const items = useSelector(selectItems);
	const dispatch = useDispatch();
	const addItem = ()=>{
		if(!currentInput){
			toast("请输入内容")
			return;
		}
		if(items.indexOf(currentInput)>-1){
			toast("不能重复添加")
			return;
		}
		dispatch(addItemAction(currentInput));
		setCurrentInput("")
	}
	return (
		<div className="itemsComponent">
			<div className="itemList">
				{
					items.map((item,idx)=>(
						<div className="item" key={item}>
							<span>{item}</span>
							<i onClick={()=>{dispatch(removeItemAction(item))}}>remove</i>
						</div>
					))
				}
			</div>
			<div className="addItem">
				<input type="text" value={currentInput} onChange={(e)=>setCurrentInput(e.target.value)} />
				<button onClick={addItem}>ADD</button>
			</div>
		</div>
	);
}
