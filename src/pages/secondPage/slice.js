import {createSlice} from '@reduxjs/toolkit';

var initialState = {
	count:0
};

export const secondPageSlice = createSlice({
	name: 'secondPage',
	initialState,
	reducers: {
		addCountAction: (state, action) => {
			state.count++
		},
		reduceCountAction: (state,action) => {
			state.count--
		},
	},
});

export const {addCountAction, reduceCountAction} = secondPageSlice.actions;
export const selectSecondPage = (state) => state.secondPage;
export default secondPageSlice.reducer;
