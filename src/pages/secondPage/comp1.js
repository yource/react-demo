import React from 'react';
import { useDispatch } from 'react-redux';
import { addCountAction } from './slice.js';
import './style.scss';

function Comp1() {
	const dispatch = useDispatch();
	
    return (
        <div className="childComp">
			<p>这里是子组件1</p>
			<button onClick={()=>{dispatch(addCountAction())}}>Add Count</button>
		</div>
    );
}

export default Comp1;
