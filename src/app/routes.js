import Login from "../pages/loginPage";
import MainLayout from "../pages/mainLayout";
import FirstPage from "../pages/firstPage";
import SecondPage from "../pages/secondPage";
import NotFoundPage from "../pages/notFound";

const routes = [{
	path: "/login",
	element: <Login />
}, {
	path: "/",
	element: <MainLayout /> ,
	children: [
		{
			path: "/firstPage",
			element: <FirstPage /> ,
		}, {
			path: "/secondPage",
			element: <SecondPage /> ,
		}
	]
}, {
	path: "*",
	element: <NotFoundPage />
}];

export default routes;