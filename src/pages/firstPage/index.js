import React from 'react';
import { useSelector } from 'react-redux';
import { changePriceAction, selectOrder} from '../../store/orderSlice.js';
import './style.scss';
import Items from "../../components/items"

function FirstPage() {
	const order = useSelector(selectOrder);
    return (
        <div className="firstPage">
			<div className="pageCon">
				<h1>FirstPage</h1>
				<div className="section section3">
					<p><span>src/index.js</span>入口文件</p>
					<p><span>src/app/app.js</span>App主体框架</p>  
					<p><span>src/app/app.scss</span>全局样式</p>  
					<p><span>src/app/routes.js</span>路由文件</p>  
					<p><span>src/components/loading&toast&dialog</span>不依赖react的ui组件，原生js操作dom</p>  
					<p><span>src/components/customDialog</span>依赖react的ui组件</p>  
					<p><span>src/components/items</span>需要跨页面引用的业务组件</p>  
					<p><span>src/pages/secondPage/index.js</span>页面入口</p>  
					<p><span>src/pages/secondPage/slice.js</span>当前页面需要共享的数据</p>  
					<p><span>src/pages/secondPage/comp1.js&comp2.js</span>只在当前页面用到的组件</p>  
					<p><span>src/store/index.js</span>引入所有（全局的、页面的、组件的）reducer生成store</p>  
					<p><span>src/store/userSlice&itemsSlice</span>需要跨页面引用的全局数据</p>
				</div>
				<div className="section section1">
					<h2>全局数据 order</h2>
					<p className="bigText">order price: {order.price}</p>
				</div>
				<div className="section section2">
					<h2>跨页面组件 items</h2>
					<Items/>
				</div>
			</div>
		</div>
    );
}

export default FirstPage;
