import "./style.scss";

var dialog = [];

/**
 * config.title 标题
 * config.content 文本内容
 * config.okText 确定按钮的文本，默认OK
 * config.okCallback 点击确定的回调
 * config.cancelText 取消按钮的文本，默认Cancel
 * config.cancelcallback 点击取消的回调
 */
function confirm(config){
	if(!config.content) return;
	var id = "dialog_"+Math.random().toString(36).substr(2, 15) + new Date().getTime().toString(36);
	var dialog = window.document.createElement("DIV");
	var close = false;
	dialog.classList.add("my_dialog_wrap");
	dialog.id = id;
	dialog.innerHTML = `<div class="my_dialog">
		<div class="my_dialog_title">${config.title||'WARNING'}</div>
		<div class="my_dialog_content">${config.content}</div>
		<div class="my_dialog_foot">
			<div class="my_dialog_btn ok">${config.okText||"OK"}</div>
			<div class="my_dialog_btn cancel">${config.cancelText||"Cancel"}</div>
		</div>
	</div>`
	
	window.document.body.appendChild(dialog);
	setTimeout(()=>{
		dialog.classList.add("show");
		window.document.querySelector("#"+id+" .my_dialog_btn.ok").addEventListener("click",()=>{
			if(close) return;
			closeDialog();
			if(config.okCallback && typeof config.okCallback=="function"){
				config.okCallback()
			}
		})
		window.document.querySelector("#"+id+" .my_dialog_btn.cancel").addEventListener("click",()=>{
			if(close) return;
			closeDialog();
			if(config.cancelcallback && typeof config.cancelcallback=="function"){
				config.cancelcallback()
			}
		})
		function closeDialog(){
			close = true;
			dialog.classList.add("hide");
			setTimeout(()=>{
				dialog.parentNode.removeChild(dialog);
			},150)
		}
	},20)
}

export default {confirm}