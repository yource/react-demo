import axios from "axios";

var instance = axios.create({
	baseURL: 'http://orderdev.menusifu.cn:3100/dashboard',
	timeout: 20000,
	headers: {
		'Content-Type': 'application/json'
	}
});

instance.interceptors.request.use(function(config) {
	var token = window.localStorage.getItem("token");
	if(token && !(config.method=="post" && config.url=="/session")){
		config.headers['Access-Token'] = token
	}
	return config;
});

instance.interceptors.response.use(function(response) {
	if(response.data){
		return response.data
	}
	return response;
}, function(error) {
	return Promise.reject(error);
});

export default instance;
