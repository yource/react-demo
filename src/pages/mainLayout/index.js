import React, { useState } from 'react';
import { Link,Outlet,useNavigate} from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { logoutAction } from "../../store/userSlice.js";
import './style.scss';
import { dialog } from "../../components"

export default function MainLayout() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const user = useSelector(state => state.user);
	const logOut = ()=>{
		dialog.confirm({
			title:"提示",
			content:"确定退出登录吗？",
			okCallback: ()=>{
				dispatch(logoutAction());
				navigate("/login")
			}
		})
	}
	return (
		<div className="mainLayout">
			<div className="mainHeader">
				<div className="topNav">
					<Link to="/firstPage">First Page</Link>
					<Link to="/secondPage">Second Page</Link>
				</div>
				<div className="user">
					<i>{user.email}</i>
					<span onClick={logOut}>Logout</span>
				</div>
			</div> 
			<div className="mainContainer">
				<Outlet />
			</div>
		</div>
	);
}
