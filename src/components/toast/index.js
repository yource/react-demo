import "./style.scss";

var hideTimer;
function toast(text,duration){
	var dom = window.document.getElementById("my_taost");
	if(!dom){
		dom = window.document.createElement("DIV");
		dom.id = "my_taost";
		dom.innerHTML = text;
		window.document.body.appendChild(dom);
	}else{
		dom.innerHTML = text;
	}
	if(hideTimer){
		window.clearTimeout(hideTimer);
	}
	hideTimer = setTimeout(()=>{
		dom.parentNode.removeChild(dom);
		hideTimer = null;
	},duration || 2800)
}

export default toast