import request from "./request.js";

export function login(data){
	return request({
		method: "POST",
		url:"/session",
		data: data
	})
}