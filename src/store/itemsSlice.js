import {createSlice} from '@reduxjs/toolkit';

var initialState = [];

export const itemsSlice = createSlice({
	name: 'items',
	initialState,
	reducers: {
		addItemAction: (state, action) => {
			if(state.indexOf(action.payload)>-1){
				alert("不能重复添加")
			}else{
				state.push(action.payload)
			}
		},
		removeItemAction: (state,action) => {
			for(var i=0;i<state.length;i++){
				if(state[i]==action.payload){
					state.splice(i,1);
					break;
				}
			}
		},
	},
});

export const {addItemAction, removeItemAction} = itemsSlice.actions;
export const selectItems = (state) => state.items;
export default itemsSlice.reducer;
