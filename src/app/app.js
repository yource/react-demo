import React,{ useEffect } from 'react';
import { useNavigate, useLocation, useRoutes } from 'react-router-dom';
import './normalize.css';
import './app.scss';
import routes from './routes'

function App() {
	let navigate = useNavigate();
	let location = useLocation();
	useEffect(()=>{
		if(location.pathname=="/" || (location.pathname!=="/login" && !localStorage.getItem("token"))){
			navigate('/login',{ replace: true })
		}else if(location.pathname=="/"){
			navigate('/firstPage',{ replace: true })
		}
	})
	let element = useRoutes(routes);
	return (
		<div className="App">
			{element}
		</div>
	);
}

export default App;
