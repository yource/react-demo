import React from 'react';
import { useDispatch } from 'react-redux';
import { reduceCountAction } from './slice.js';
import './style.scss';

function Comp2() {
	const dispatch = useDispatch();
	
    return (
        <div className="childComp">
			<p>这里是子组件2</p>
			<button onClick={()=>{dispatch(reduceCountAction())}}>Reduce Count</button>
		</div>
    );
}

export default Comp2;
